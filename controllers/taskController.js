const Task = require("../models/task");

module.exports.getAllTasks = () => {
	return Task.find({}).then((result) => {
		return result;
	})
}

/* The request body coming from the client was passed from the "taskroute.js" file
via "req.body" as an argument and is renamed a "requestBody" parameter in the controller file */

module.exports.createTask = (requestBody) => {

	// Creates a task object based on the Mongoose model "task"

	let newTask = new Task({

		// Sets the "name" property with the value received from the client/postman
		name : requestBody.name
	})

	return newTask.save().then((task, error) => {
		if (error) {
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
}

module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		} else {
			return removedTask;
		}
	})
}

module.exports.updateTask = (taskId, newContent) => { 

	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		} 
		
		result.name = newContent.name;

		return result.save().then((updateTask, saveErr) => {
			if (saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updateTask;
			}
		})
	})
}

module.exports.getAllTasks = (taskId) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error)
			return false;
		} else {
			return result;
		}
	})
}

module.exports.updateTask = (taskId, newContent) => { 

	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		} 
		
		result.status = newContent.status;

		return result.save().then((updateTask, saveErr) => {
			if (saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updateTask;
			}
		})
	})
}
